import json

from ..common.extensions import db
from sqlalchemy import DateTime


class ExpenseModel(db.Model):
    __tablename__ = 'expenses'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=True)
    category = db.Column(db.String(100), nullable=True)
    amount = db.Column(db.Float(precision=2), nullable=False)
    date = db.Column(db.String(100), nullable=False)

    def __init__(self, name, category, amount, date):
        self.name = name
        self.category = category
        self.amount = amount
        self.date = date

    def json(self):
        return {'name': self.name, 'category': self.category, 'amount': self.amount, 'date': self.date}

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()
