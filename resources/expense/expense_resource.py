from ...common.extensions import Api, Resource, reqparse
from ...models.expense import ExpenseModel
from datetime import datetime

expense_api = Api()


class Expense(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('amount', type=float, required=True, help="Item amount can't be left blank")
    parser.add_argument('name', type=str, required=False)
    parser.add_argument('category', type=str, required=False)

    def get(self):
        return {'message': 'work in-progress'}, 200

    def post(self):
        data = Expense.parser.parse_args()
        print(type(data.name))
        expense = ExpenseModel(data.name, data.category, data.amount, str(datetime.utcnow()))
        try:
            expense.save_to_db()
        except:
            raise
        return expense.json(), 201


expense_api.add_resource(Expense, '/api/expenses')
