from flask import Flask
from .common.extensions import db
from .resources.expense.expense_resource import expense_api


def create_app():
    app = Flask(__name__)
    app.config.from_object('expense-tracker-api.config.Config')
    expense_api.init_app(app)
    db.init_app(app)
    return app
